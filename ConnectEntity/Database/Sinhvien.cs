namespace ConnectEntity.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sinhvien")]
    public partial class Sinhvien
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Hoten { get; set; }

        public DateTime? Ngaysinh { get; set; }

        [StringLength(50)]
        public string Quequan { get; set; }

        public int? id_lophoc { get; set; }

        public virtual Lophoc Lophoc { get; set; }
    }
}
