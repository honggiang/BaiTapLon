namespace ConnectEntity.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lophoc")]
    public partial class Lophoc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lophoc()
        {
            Sinhvien = new HashSet<Sinhvien>();
        }

        [Key]
        public int ID_Lophoc { get; set; }

        [Column("Lophoc")]
        [StringLength(50)]
        public string Lophoc1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sinhvien> Sinhvien { get; set; }
    }
}
