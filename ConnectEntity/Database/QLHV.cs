namespace ConnectEntity.Database
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class QLHV : DbContext
    {
        public QLHV()
            : base("name=QLHV")
        {
        }

        public virtual DbSet<Lophoc> Lophoc { get; set; }
        public virtual DbSet<Sinhvien> Sinhvien { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
