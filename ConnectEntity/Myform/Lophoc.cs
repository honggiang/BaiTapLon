﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using ConnectEntity.Database;

namespace ConnectEntity.Myform
{
    public partial class Lophoc : Form
    {
        QLHV ql = new QLHV();
        public Lophoc()
        {
            InitializeComponent();
        }

        private void Lophoc_Load(object sender, EventArgs e)
        {
            ql.Lophoc.Load();
            lophocBindingSource.DataSource = ql.Lophoc.Local.ToBindingList();
        }

        private void lophocBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                bindingNavigatorPositionItem.Focus();
                lophocBindingSource.EndEdit();
                ql.SaveChanges();
                MessageBox.Show("Done");
            }
            catch 
            {
                MessageBox.Show("Lỗi thêm dữ liệu!!!");
            }
        }
    }
}
