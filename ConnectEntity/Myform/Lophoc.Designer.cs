﻿namespace ConnectEntity.Myform
{
    partial class Lophoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lophoc1Label;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lophoc));
            this.lophocBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lophocBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lophocBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.lophocGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID_Lophoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLophoc1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lophoc1TextEdit = new DevExpress.XtraEditors.TextEdit();
            lophoc1Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lophocBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lophocBindingNavigator)).BeginInit();
            this.lophocBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lophocGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lophoc1TextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lophoc1Label
            // 
            lophoc1Label.AutoSize = true;
            lophoc1Label.Location = new System.Drawing.Point(248, 262);
            lophoc1Label.Name = "lophoc1Label";
            lophoc1Label.Size = new System.Drawing.Size(31, 13);
            lophoc1Label.TabIndex = 2;
            lophoc1Label.Text = "Lớp :";
            // 
            // lophocBindingSource
            // 
            this.lophocBindingSource.DataSource = typeof(ConnectEntity.Database.Lophoc);
            // 
            // lophocBindingNavigator
            // 
            this.lophocBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.lophocBindingNavigator.BindingSource = this.lophocBindingSource;
            this.lophocBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.lophocBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.lophocBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.lophocBindingNavigatorSaveItem});
            this.lophocBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.lophocBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.lophocBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.lophocBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.lophocBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.lophocBindingNavigator.Name = "lophocBindingNavigator";
            this.lophocBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.lophocBindingNavigator.Size = new System.Drawing.Size(442, 25);
            this.lophocBindingNavigator.TabIndex = 0;
            this.lophocBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(58, 22);
            this.bindingNavigatorAddNewItem.Text = "Thêm";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(47, 22);
            this.bindingNavigatorDeleteItem.Text = "Xóa";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // lophocBindingNavigatorSaveItem
            // 
            this.lophocBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("lophocBindingNavigatorSaveItem.Image")));
            this.lophocBindingNavigatorSaveItem.Name = "lophocBindingNavigatorSaveItem";
            this.lophocBindingNavigatorSaveItem.Size = new System.Drawing.Size(62, 22);
            this.lophocBindingNavigatorSaveItem.Text = "Lưu lại";
            this.lophocBindingNavigatorSaveItem.Click += new System.EventHandler(this.lophocBindingNavigatorSaveItem_Click);
            // 
            // lophocGridControl
            // 
            this.lophocGridControl.DataSource = this.lophocBindingSource;
            this.lophocGridControl.Location = new System.Drawing.Point(0, 28);
            this.lophocGridControl.MainView = this.gridView1;
            this.lophocGridControl.Name = "lophocGridControl";
            this.lophocGridControl.Size = new System.Drawing.Size(410, 225);
            this.lophocGridControl.TabIndex = 1;
            this.lophocGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID_Lophoc,
            this.colLophoc1});
            this.gridView1.GridControl = this.lophocGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            // 
            // colID_Lophoc
            // 
            this.colID_Lophoc.Caption = "ID";
            this.colID_Lophoc.FieldName = "ID_Lophoc";
            this.colID_Lophoc.Name = "colID_Lophoc";
            this.colID_Lophoc.Visible = true;
            this.colID_Lophoc.VisibleIndex = 0;
            // 
            // colLophoc1
            // 
            this.colLophoc1.Caption = "Trung đội";
            this.colLophoc1.FieldName = "Lophoc1";
            this.colLophoc1.Name = "colLophoc1";
            this.colLophoc1.Visible = true;
            this.colLophoc1.VisibleIndex = 1;
            // 
            // lophoc1TextEdit
            // 
            this.lophoc1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.lophocBindingSource, "Lophoc1", true));
            this.lophoc1TextEdit.Location = new System.Drawing.Point(294, 259);
            this.lophoc1TextEdit.Name = "lophoc1TextEdit";
            this.lophoc1TextEdit.Size = new System.Drawing.Size(100, 20);
            this.lophoc1TextEdit.TabIndex = 3;
            // 
            // Lophoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 325);
            this.Controls.Add(lophoc1Label);
            this.Controls.Add(this.lophoc1TextEdit);
            this.Controls.Add(this.lophocGridControl);
            this.Controls.Add(this.lophocBindingNavigator);
            this.Name = "Lophoc";
            this.Text = "Lophoc";
            this.Load += new System.EventHandler(this.Lophoc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lophocBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lophocBindingNavigator)).EndInit();
            this.lophocBindingNavigator.ResumeLayout(false);
            this.lophocBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lophocGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lophoc1TextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource lophocBindingSource;
        private System.Windows.Forms.BindingNavigator lophocBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton lophocBindingNavigatorSaveItem;
        private DevExpress.XtraGrid.GridControl lophocGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colID_Lophoc;
        private DevExpress.XtraGrid.Columns.GridColumn colLophoc1;
        private DevExpress.XtraEditors.TextEdit lophoc1TextEdit;
    }
}